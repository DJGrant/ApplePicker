﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScript : MonoBehaviour {

	Camera cam;
	float startDistance;

	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();
		startDistance = cam.transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		var newFrustHeight = 9.6f / cam.aspect;
		if (newFrustHeight < 4.4f)
			newFrustHeight = 4.4f;

		var distance = newFrustHeight * 0.5f / Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);

		cam.fieldOfView = 2.0f * Mathf.Atan((newFrustHeight * 0.5f) / distance) * Mathf.Rad2Deg;

		transform.position = new Vector3 (transform.position.x, transform.position.y, distance);
	}
}
