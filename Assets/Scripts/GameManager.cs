﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager gm;

	public enum gameStates {Playing, GameOver, Title};
	public gameStates gameState = gameStates.Title;

	public int score = 0;
	public int initialLives = 5;
	int lives;
	GameObject[] lifeGraphics;
	GameObject livesBG;
	public GameObject LifeGraphic;
	public GameObject DeathGraphic;
	public GameObject LifeBackgroundGraphic;

	public GameObject AppleSpawner;

	public GameObject MainCanvas;
	public Text ScoreDisplay;
	public GameObject GameOverCanvas;
	public Text EndScoreDisplay;
	public GameObject TitleCanvas;

	// Use this for initialization
	void Start () {
		if (gm == null)
			gm = gameObject.GetComponent<GameManager> ();

		TitleCanvas.SetActive (true);
		MainCanvas.SetActive (false);
		GameOverCanvas.SetActive (false);

		initializeLives ();
	}
	
	// Update is called once per frame
	void Update () {
		switch (gameState) {
		case gameStates.Playing:
			if (lives <= 0) {
				MainCanvas.SetActive (false);
				GameOverCanvas.SetActive (true);
				EndScoreDisplay.text = "Your Score: " + score;
				EndScoreDisplay.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Your Score: " + score;
				gameState = gameStates.GameOver;

				Cursor.visible = true;
			}
			break;
		case gameStates.GameOver:
			break;
		case gameStates.Title:
			break;
		}
	}

	public void Collect (int amount) {
		score += amount;
		gm.ScoreDisplay.text = score.ToString();
	}

	public void loseLife () {
		if (lives > 0) {
			Destroy (lifeGraphics [lives - 1]);
			GameObject death = instantiateLifeGraphic (DeathGraphic, lives - 1);
			lifeGraphics [lives - 1] = death;
			lives--;
		}
	}

	public void initializeLives () {
		lives = initialLives;

		livesBG = Instantiate (LifeBackgroundGraphic);
		livesBG.transform.SetParent (MainCanvas.transform);
		RectTransform livesBGRect = livesBG.GetComponent<RectTransform> ();
		livesBGRect.anchoredPosition = new Vector3 (11.25f, -32f, 0f);
		livesBGRect.sizeDelta = new Vector2(55f + (42.5f* (initialLives-1)), livesBGRect.rect.height);
		livesBG.transform.localScale = new Vector3 (0.75f,0.75f,1f);

		lifeGraphics = new GameObject[initialLives];
		for (int i = 0; i < initialLives; i++) {
			lifeGraphics[i] = instantiateLifeGraphic (LifeGraphic, i);
		}
	}

	public void replay () {
		Cursor.visible = false;

		if (livesBG != null)
			Destroy (livesBG);
		foreach (GameObject obj in lifeGraphics) {
			if (obj != null)
				Destroy (obj);
		}
		initializeLives ();
		score = 0;
		gm.Collect (0);
		AppleSpawner.GetComponent<AppleSpawner>().ResetSpawner();

		GameOverCanvas.SetActive (false);
		MainCanvas.SetActive (true);

		gameState = gameStates.Playing;
	}

	public void startGame () {
		Cursor.visible = false;

		TitleCanvas.GetComponent<Animator> ().SetTrigger ("Start");
		GameOverCanvas.SetActive (false);
		MainCanvas.SetActive (true);

		score = 0;
		gm.Collect (0);

		gameState = gameStates.Playing;
	}

	GameObject instantiateLifeGraphic (GameObject graphic, int place) {
		GameObject life = Instantiate (graphic);
		life.transform.SetParent (MainCanvas.transform);
		life.GetComponent<RectTransform>().anchoredPosition = new Vector3 (32f + (32f * place), -32f, 0f);
		life.transform.localScale = new Vector3 (1f,1f,1f);
		return life;
	}
}
