﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleSpawner : MonoBehaviour {

	public float Width = 7.4f;
	public float Height = 0.8f;

	float leftBound;
	float rightBound;
	float topBound;
	float bottomBound;

	public float initialSpawnSpeed = 2f;
	public float fastestSpawnSpeed = 0.5f;
	float spawnSpeed; // Current time between apple spawns
	float countdown; // Countdown to the next apple spawn

	Vector2 applePos; // Position of apple being spawned
	public float initialOffsetRadius = 7.4f;
	public float minOffsetRadius = 1.5f;
	float offsetRadius;

	public float initialGravity = -1f;
	public float maxGravity = -8f;

	public GameObject apple;

	// Use this for initialization
	void Start () {
		ResetSpawner ();

		leftBound = transform.position.x - (Width / 2);
		rightBound = transform.position.x + (Width / 2);
		topBound = transform.position.y + (Height / 2);
		bottomBound = transform.position.y - (Height / 2);
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.gm.gameState == GameManager.gameStates.Playing) {
			if (countdown <= 0f && apple != null) {
				countdown = spawnSpeed;
				Instantiate (apple, applePos, Quaternion.identity);

				calculateNextApplePos ();
			}
			countdown -= Time.deltaTime;

			if (spawnSpeed > fastestSpawnSpeed)
				spawnSpeed -= 0.0005f;
			else
				spawnSpeed = fastestSpawnSpeed;

			if (Physics.gravity.y > maxGravity)
				Physics.gravity += new Vector3 (0f, -0.001f, 0f);
			else
				Physics.gravity = new Vector3 (0f, maxGravity, 0f);

			if (offsetRadius > minOffsetRadius)
				offsetRadius -= 0.002f;
			else
				offsetRadius = minOffsetRadius;
		}
	}

	void calculateNextApplePos() {
		Vector2 appleOffset = new Vector2(Random.Range(-offsetRadius, offsetRadius), Random.Range(-offsetRadius/4, offsetRadius/4));

		/*if (!isInBoundsX(applePos + appleOffset))
			appleOffset = new Vector2 (0f, appleOffset.y);
		if (!isInBoundsY(applePos + appleOffset))
			appleOffset = new Vector2 (appleOffset.x, 0f);*/

		Debug.Log ("("+applePos.x+", "+applePos.y+")");
		applePos += appleOffset;
		applePos = new Vector2 (Mathf.Clamp(applePos.x, leftBound, rightBound), Mathf.Clamp(applePos.y, bottomBound, topBound));
	}

	public void ResetSpawner()
	{
		spawnSpeed = initialSpawnSpeed;
		countdown = spawnSpeed;
		applePos = transform.position;
		offsetRadius = initialOffsetRadius;

		Physics.gravity = new Vector3 (0f, initialGravity, 0f);
	}
}
