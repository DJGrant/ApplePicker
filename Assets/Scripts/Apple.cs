﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apple : MonoBehaviour {

	Rigidbody rb;
	SphereCollider collider;
	//bool falling = false;
	public float waitTime; // Time apple waits before falling
	public GameObject splatAnimation;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		collider = GetComponent<SphereCollider> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.gm.gameState == GameManager.gameStates.GameOver) {
			if (splatAnimation != null)
				Instantiate(splatAnimation, transform.position, Quaternion.identity);
			Destroy (gameObject);
		}

		if (waitTime <= 0) {
			rb.useGravity = true;
			Random.InitState(System.Environment.TickCount);
			rb.AddTorque (Random.value, Random.value, Random.value);
		} else {
			waitTime -= Time.deltaTime;
		}
	}

	void OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "Ground") {
			GameManager.gm.loseLife ();
			if (splatAnimation != null)
				Instantiate(splatAnimation, transform.position, Quaternion.identity);
			Destroy (gameObject);
		}
	}
}
