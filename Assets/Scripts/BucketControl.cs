﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketControl : MonoBehaviour {

	Vector3 movement;
	Vector3 lastPos;
	Rigidbody rb;
	Camera cam;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		cam = Camera.main;

		lastPos = transform.position;
	}
	
	// Update is called once per frame, but this is FixedUpdate, which is different
	void FixedUpdate () {
		Vector3 newPos = cam.ScreenToWorldPoint (new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.transform.position.z));
		rb.MovePosition (new Vector3(newPos.x, transform.position.y, newPos.z));

		float newZRotation = Mathf.Clamp(50 * (lastPos.x - transform.position.x), -10, 10);
		rb.MoveRotation (Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, newZRotation)), 0.3f));

		lastPos = transform.position;
	}
}
